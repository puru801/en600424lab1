from twisted.internet.protocol import Protocol, Factory
from twisted.internet.endpoints import TCP4ServerEndpoint
from twisted.internet import reactor, defer
from sys import stdout


class Echo(Protocol):

    def dataReceived(self, data):
        print data
        self.transport.write(data)


class EchoFactory(Factory):

    def buildProtocol(self, addr):
        print 'New Client connected: ',addr
        return Echo()

endpoint = TCP4ServerEndpoint(reactor, 1234)
endpoint.listen(EchoFactory())
reactor.run()
