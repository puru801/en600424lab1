from twisted.internet.protocol import Protocol, Factory
from twisted.internet.endpoints import TCP4ClientEndpoint, connectProtocol
from twisted.internet import reactor
import sys


class EchoClient(Protocol):
    
    def dataReceived(self, data):
        print "Response: ", data

    def sendMessage(self, data):
        self.transport.write(data)

    def connectionLost(self, reason):
        reactor.stop()


def getstuff(p, data):
    reactor.callLater(0, p.sendMessage, data)
    reactor.callLater(1, p.transport.loseConnection)


endpoint = TCP4ClientEndpoint(reactor, "127.0.0.1", 1234)
d = connectProtocol(endpoint, EchoClient())
d.addCallback(getstuff, sys.argv[1])
reactor.run()
