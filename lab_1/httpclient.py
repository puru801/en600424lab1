from twisted.internet.protocol import Protocol, Factory
from playground.twisted.endpoints import GateClientEndpoint
from twisted.internet.endpoints import connectProtocol
from twisted.internet import reactor
import sys
import time


class HTTPClient(Protocol):
    def dataReceived(self, data):
        data = data.split("\r\n")
        header = data[0]
        headerterms = header.split()
        if (headerterms[1]=="200"):
        	page = data[len(data)-1]
        	print page
        elif(headerterms[1]=="404"):
        	print "\n404: Not Found\n"
        elif(headerterms[1]=="400"):
        	print "\n400: Bad Request\n"
        else:
        	print header

    def sendRequest(self, data):
        if("/" not in data):
        	data = data+"/"
        	
        localtime = time.asctime(time.localtime(time.time()))
        Httpreq = "GET "+data+" HTTP/1.1\r\nHost: 127.0.0.1\r\nDate: "+localtime+" EST\r\n\r\n"

        self.transport.write(Httpreq)

    def connectionLost(self, reason):
        reactor.stop()


def getstuff(p, data):
    reactor.callLater(0, p.sendRequest, data)
    reactor.callLater(1, p.transport.loseConnection)

if (len(sys.argv)>2):
	print "Too many arguments!\nUSAGE: $ python httpclient.py <<requested page>>"
	exit()
elif (len(sys.argv)<2):
	print 'Too few arguments!\nUSAGE: $ python httpclient.py <<requested page>>'
	exit()

endpoint = GateClientEndpoint.CreateFromConfig(reactor, "20164.0.0.1", 80)
d = connectProtocol(endpoint, HTTPClient())
d.addCallback(getstuff, sys.argv[1])
reactor.run()
