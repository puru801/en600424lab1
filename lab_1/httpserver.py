from twisted.internet.protocol import Protocol, Factory
from playground.twisted.endpoints import GateServerEndpoint
from twisted.internet import reactor, defer
from sys import stdout
import os
import time


class HTTPServer(Protocol):

    def dataReceived(self, data):
        localtime = time.asctime(time.localtime(time.time()))
        headerOK = "HTTP/1.1 200 OK\r\nDate: "+localtime+" EST\r\n\r\n"
        Header404="HTTP/1.1 404 Not Found\r\nDate: "+localtime+" EST\r\n\r\n"
        Header400 = "HTTP/1.1 400 Bad Request\r\nDate: "+localtime+" EST\r\n\r\n"
        test = data.splitlines()
        req = test[0].split()
        if(test[0].startswith('GET') and (req[2]=="HTTP/1.1" or req[2]=="HTTP/1.0") and test[len(test)-1]==''):
            if(os.path.isdir(req[1])):
                path = req[1]+"index.html"
                f = open(path, "r+")
                page = f.read()
                resp = headerOK + page
            elif(os.path.exists(req[1])):
                f = open(req[1], "r+")
                page = f.read()
                resp = headerOK + page
            else:
                resp = Header404
        else:
            resp = Header400

        self.transport.write(resp)



class HTTPFactory(Factory):

    def buildProtocol(self, addr):
        print 'New Client connected ', addr
        return HTTPServer()

endpoint = GateServerEndpoint.CreateFromConfig(reactor, 80)
endpoint.listen(HTTPFactory())
reactor.run()
